
## Inference Engine (IC-IA)

Esse projeto constitui numa simples máquina de inferencia que recebe expressões no formato de regras "P → Q" e "P", **afirmando se uma determinada hipótese S é verdadeira ou não**.
O programa imprime o resultado da análise e no caso de soluções de hipóteses verdadeiras, **imprime o passo-a-passo da prova**.

Na lógica proposicional, **modus ponendo ponens** (em latim significa "a maneira que afirma afirmando") ou a eliminação da implicação é uma válida e simples forma de argumento e regra de inferência. Ele pode ser resumido como "P implica Q, P é afirmado verdade, portanto, Q deve ser verdade."

Sendo um dos conceitos mais utilizados na lógica, ele constitui num dos mecanismos aceitos para a construção de provas dedutivas, que inclui a "regra de definição" e a "regra de substituição".
## Executando o projeto

O projeto é estruturado de forma simples sendo necessário apenas executar o módulo principal `InferenceEngine` contido no arquivo`InferenceEngine.java`.

## Exemplo de execução projeto
```
## Expression format: (X -> Y) or (X)

(Insert 0 to stop) Enter the expression: A -> B
(Insert 0 to stop) Enter the expression: B -> C
(Insert 0 to stop) Enter the expression: A
(Insert 0 to stop) Enter the expression: 0
What is the hypothesis: C

[ TRUE HYPOTHESIS ]

A -> B
A
------
B
============
B -> C
B
------
C
============

```
