package br.ic.ufal.ia;

import java.util.List;

public class Evaluator {
    private List<Expression> expressions;
    private char hypothesis;
    private Expression currentAtom;
    private String answer = "";
    
    Evaluator(List<Expression> expressions, char hypothesis){
        this.expressions = expressions;
        this.hypothesis = hypothesis;
    }

    void evaluate() {
        //Get current atom
        this.currentAtom = findAtomMatch();

        if(this.currentAtom == null){ // If an atom was not found
            System.out.println("\n[ FALSE HYPOTHESIS ]");
        } else if(this.currentAtom.getAlpha() == this.hypothesis){ // If currentAtom is equal to the hypothesis
            System.out.println("\n[ TRUE HYPOTHESIS ]");
            System.out.println(this.answer);
        } else {
            // remove atom from list
            this.expressions.remove(this.currentAtom);
            // iterate & find inference match
            Expression inferenceResult = findInferenceMatch();
            if(inferenceResult != null){
                // remove inference from list
                this.expressions.remove(inferenceResult);
                // add result to the list
                this.expressions.add(new Expression(inferenceResult.getBeta()));
            }

            this.evaluate();
        }
    }

    private Expression findAtomMatch(){
        for (Expression expression : this.expressions) {
            if (expression.atom) {
                return expression;
            }
        }
        return null;
    }

    private Expression findInferenceMatch(){
        //Compare expression
        Expression expressionItem;
        for (Expression expression : this.expressions) {
            expressionItem = expression;
            if (this.currentAtom.getAlpha() == expressionItem.getAlpha()) {
                // Build answer
                this.answer += "\n"
                        + expressionItem.getAlpha() + " -> " + expressionItem.getBeta() + "\n"
                        + this.currentAtom.getAlpha() + "\n"
                        + "------\n"
                        + expressionItem.getBeta()
                        + "\n============";
                return expressionItem;
            }
        }
        return null;
    }

    public List<Expression> getExpressions() {
        return expressions;
    }

    public String getAnswer() {
        return answer;
    }
}
