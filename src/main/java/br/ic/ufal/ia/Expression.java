package br.ic.ufal.ia;

public class Expression {

    private char alpha;
    private String operator;
    private char beta;
    final boolean atom;

    Expression(String expString){
        String[] split = expString.split(" ");
        this.alpha  = split[0].charAt(0);
        this.operator  = split[1];
        this.beta  = split[2].charAt(0);
        this.atom = false;
    }

    Expression(char expString){
        this.alpha = expString;
        this.atom = true;
    }

    char getAlpha() {
        return alpha;
    }

    char getBeta() {
        return beta;
    }

    @Override
    public String toString() {
        return "br.ic.ufal.ia.Expression{" +
                "alpha=" + alpha +
                ", operator='" + operator + '\'' +
                ", beta=" + beta +
                ", atom=" + atom +
                '}';
    }
}
