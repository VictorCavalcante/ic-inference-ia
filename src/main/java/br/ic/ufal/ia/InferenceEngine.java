package br.ic.ufal.ia;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InferenceEngine {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        // Get user input
        List<Expression> expressions = getExpressionsInput(reader);
        char hypothesis = getHypothesisInput(reader);
        reader.close();

        // Evaluate expressions
        Evaluator evaluator = new Evaluator(expressions, hypothesis);
        evaluator.evaluate();
    }

    private static List<Expression> getExpressionsInput(Scanner reader) {
        List<Expression> expressions = new ArrayList<>();
        String userInput = "";

        System.out.println("## Expression format: (X -> Y) or (X)\n");
        while(true) {
            System.out.print("(Insert 0 to stop) Enter the expression: ");
            userInput = reader.nextLine();
            if (userInput.equals("0")) { break; }

            if(userInput.length() == 1 ){
                expressions.add(new Expression(userInput.charAt(0)));
            } else {
                expressions.add(new Expression(userInput));
            }
        }

        return expressions;
    }

    private static char getHypothesisInput(Scanner reader) {
        String userInput = "";

        System.out.print("What is the hypothesis: ");
        userInput = reader.nextLine();

        return userInput.charAt(0);
    }
}
